import spacy

# Carica il modello in lingua italiana
nlp = spacy.load("it_core_news_sm")

# Definisci il testo da tokenizzare
testo = "Questo è un esempio di tokenizzazione di un testo in Python con spaCy."

# Crea un oggetto Doc dal testo
doc = nlp(testo)

# Stampa ogni token del testo
for token in doc:
    print(token.text)
