import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer

# Crea un dataset di esempio
documenti = ['Il gatto correva dietro il cane', 
             'Il cane è veloce', 
             'Il gatto è pigro']

# Crea un dataframe
df = pd.DataFrame({'documenti': documenti})

# Crea un oggetto TfidfVectorizer
tfidf = TfidfVectorizer()

# Calcola i valori di TF-IDF per ciascun documento
matrice_tfidf = tfidf.fit_transform(df['documenti'])

# Raccogli le features (i nomi analizzati)
features = tfidf.get_feature_names_out()

# Crea un DataFrame per visualizzare i pesi TF-IDF
df_tfidf = pd.DataFrame(matrice_tfidf.todense(), columns = features)

# Stampa le prime 5 righe del DataFrame
print(df_tfidf.head())
