import spacy

# Carica il modello di lingua italiana
nlp = spacy.load("it_core_news_sm")

# Definisci il testo da stemmare
testo = "La corsa del cane era molto veloce. Il gatto correva dietro di lui."

# Processa il testo con spacy
doc = nlp(testo)

# Stemma ogni parola del testo
parole_stemmate = [token.text.lower() for token in doc if not token.is_punct | token.is_space]

# Stampa le parole stemmate
print(parole_stemmate)
