import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')

stop_words = set(stopwords.words('italian'))

def remove_stop_words(text):
    words = nltk.word_tokenize(text)
    words = [word.lower() for word in words if word.isalpha()]
    words = [word for word in words if word not in stop_words]
    return words

text = "Questo è un esempio di pulizia del testo usando la libreria NLTK in Python. Rimuove le stop words dal testo."

print(remove_stop_words(text))