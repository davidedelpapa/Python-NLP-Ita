import nltk
from nltk.corpus import stopwords

# Scaricare la lista delle stop-word
nltk.download('stopwords')

# Estrarre la lista delle stop-word in inglese
stop_words = set(stopwords.words('italian'))

# Esempio di testo da pulire
text = "Questo è un esempio di pulizia del testo usando la libreria NLTK in Python. Rimuove le stop words dal testo."

# Dividi il testo in parole
words = nltk.word_tokenize(text)

# Rimuovi le stop-word dal testo
filtered_words = [word for word in words if word.lower() not in stop_words]

# Unisci le parole pulite in un testo
cleaned_text = " ".join(filtered_words)

print("Testo originale:", text)
print("Testo pulito:", cleaned_text)