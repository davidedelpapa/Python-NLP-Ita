from sklearn.feature_extraction.text import CountVectorizer

# Definire i documenti
documenti = ["Il gatto corre velocemente.", "Il cane è veloce.", "Il gatto è molto veloce."]

# Creare un'istanza di CountVectorizer
vectorizer = CountVectorizer()

# Trasformare i documenti in una matrice BOW
bow = vectorizer.fit_transform(documenti)

# Stampa le features (parole) presenti nel vocabolario
print(vectorizer.get_feature_names_out())

# Stampa la matrice BOW
print(bow.toarray())
