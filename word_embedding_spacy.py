import spacy

# Carica un modello di lingua pre-addestrato in spacy
nlp = spacy.load("it_core_news_sm")

# Definisci il testo
testo = "La mia giornata è stata molto intensa. Ho dovuto lavorare molto al computer."

# Analizza il testo usando il modello di lingua pre-addestrato
doc = nlp(testo)

# Stampa le rappresentazioni di word embedding delle parole
for token in doc:
    print(token.text, token.vector[:5])

# Stampa il vettore per la parola in posizione 2, "giornata"
print(doc[2].vector)
