import spacy

# Carica il modello di lingua in spacy
nlp = spacy.load("it_core_news_sm")

# Definisci il testo da pulire
testo = "Questo è un esempio di testo con numeri, punteggiatura e caratteri non alfabetici. 1234567890"

# Crea un oggetto Doc da una stringa di testo
doc = nlp(testo)

# Itera su ogni token nel documento
tokens_puliti = [token.text for token in doc if not token.is_punct | token.is_space | token.is_digit | token.is_stop]

# Unisci i token puliti in una stringa di testo
testo_pulito = " ".join(tokens_puliti)

# Stampa il testo pulito
print(testo_pulito)
