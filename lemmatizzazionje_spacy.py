import spacy

# Carica il modello di Spacy
nlp = spacy.load("it_core_news_sm")

# Definisci il testo da lemmatizzare
testo = "La corsa del cane era molto veloce. Il gatto correva dietro di lui."

# Crea un oggetto di tipo Doc dal testo
doc = nlp(testo)

# Lemmatizza ogni token del documento
lemmi = [token.lemma_ for token in doc]

# Stampa i lemmi
print(lemmi)
