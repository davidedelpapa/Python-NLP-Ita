from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import make_pipeline
from sklearn.metrics import accuracy_score

# Scarichiamo i dati necessari
data = fetch_20newsgroups()

# Stampiamo l'elenco completo delle categorie
#print(data.target_names)

# Selezioniamo solo alcune categorie
categorie = ['talk.religion.misc', 'soc.religion.christian', 'sci.space', 'comp.graphics']

# Selezioniamo i sottogruppi del dataset per il training e i test per il modello
train = fetch_20newsgroups(subset='train', categories=categorie)
test = fetch_20newsgroups(subset='test', categories=categorie)

# Creiamo una pipeline
modello = make_pipeline(TfidfVectorizer(), MultinomialNB())

# Facciamo il training del modello
modello.fit(train.data, train.target)

# Applichiamo il modello per predire i dati del sottogruppo test
labels = modello.predict(test.data)

# Calcolare l'accuratezza delle previsioni
accuracy = accuracy_score(test.target, labels)
print("Accuratezza: ", accuracy)

# Possiamo applicare ilmodello per predire anche qualche argomento inventato da noi
# Di seguito alcuni esempi:

prediction = modello.predict(["Sending a shuttle to the ISS"])
print(train.target_names[prediction[0]])

prediction = modello.predict(["Discussing Atheism vs Christianism"])
print(train.target_names[prediction[0]])

