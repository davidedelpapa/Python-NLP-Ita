from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.pipeline import make_pipeline
from sklearn.datasets import fetch_20newsgroups

# Scarichiamo i dati necessari
data = fetch_20newsgroups()


# Selezioniamo solo alcune categorie
categorie = ['talk.religion.misc', 'soc.religion.christian', 'sci.space', 'comp.graphics']

# Selezioniamo i sottogruppi del dataset per il training e i test per il modello
train = fetch_20newsgroups(subset='train', categories=categorie)
test = fetch_20newsgroups(subset='test', categories=categorie)

# Creiamo una pipeline
modello = make_pipeline(TfidfVectorizer(), LogisticRegression(random_state=42))

# Facciamo il training del modello
modello.fit(train.data, train.target)

# Fare le previsioni sul set di test
labels = modello.predict(test.data)

# Calcolare l'accuratezza delle previsioni
accuracy = accuracy_score(test.target, labels)
print("Accuratezza: ", accuracy)
