import gensim
from gensim.models import Word2Vec

# Crea un modello di word2vec
model = Word2Vec([['cane', 'gatto', 'uomo', 'donna'], ['casa', 'edificio', 'strada', 'città']], min_count=1)

# Visualizza le parole più simili a 'cane'
parole_simili = model.wv.most_similar(positive=['cane'], topn=5)
print(parole_simili)