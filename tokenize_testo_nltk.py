import nltk
nltk.download('punkt')
from nltk.tokenize import word_tokenize

text = "Questo è un esempio di testo che voglio tokenizzare."
tokens = word_tokenize(text)
print(tokens)