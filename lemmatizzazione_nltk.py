import nltk
from nltk.stem import WordNetLemmatizer

# Scarica WordNet
nltk.download('wordnet')

# Crea un'istanza di WordNetLemmatizer
lemmatizer = WordNetLemmatizer()

# Definisci il testo da lemmatizzare
text = "The dog's run was very fast. The cat ran after it."

# Tokenizza il testo
parole = nltk.word_tokenize(text)

# Lemmatizza ogni parola del testo
parole_lemmatizzate = [lemmatizer.lemmatize(parola) for parola in parole]

# Stampa le parole lemmatizzate
print(parole_lemmatizzate)
