import gensim

# Definisci il testo
testo = "Questa è una prova di word embedding. Stiamo cercando di convertire le parole in vettori di significato."

# Tokenizza il testo in parole
parole = testo.split()

# Crea un modello word2vec
modello = gensim.models.Word2Vec([parole], min_count=1, vector_size=100)

# Accesso alla rappresentazione di una parola come vettore
vettore = modello.wv['parole']

# Stampa la rappresentazione del vettore
print(vettore)

# Ottieni le 5 parole più simili
simili = modello.wv.most_similar('parole', topn=5)

# Stampa le 5 parole più simili
print(simili)
