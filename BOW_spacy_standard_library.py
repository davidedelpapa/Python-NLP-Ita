import spacy
import collections

nlp = spacy.load("it_core_news_sm")

# Definisci il testo da pulire e rappresentare come BOW
documenti = ["Il gatto corre velocemente.", "Il cane è veloce.", "Il gatto è molto veloce."]
testo = " ".join(documenti)
# Pulisci il testo utilizzando Spacy
doc = nlp(testo)

# Rimuovi i token che non sono parole o che sono stop-word e lemmatizza
lemmi = [token.lemma_ for token in doc if token.is_alpha and not token.is_stop]

# Crea una rappresentazione BOW del testo
bow = collections.Counter(lemmi)

# Stampa il BOW
print(bow)
