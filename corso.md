## Argomenti

Introduzione a NLP e Python
Preparazione dei dati: tokenizzazione, pulizia del testo, eliminazione delle stop-word
Representation del testo: BOW, TF-IDF, word embeddings
Classificazione del testo: Naive Bayes, SVM, RNN
Part of speech tagging
Named entity recognition
Analisi del sentimento
Summarization del testo
Modelli di seq2seq e generazione del testo

## Introduzione a NLP e Python

L'obiettivo di questa introduzione è fornire una base solida per gli argomenti successivi del corso e preparare gli studenti a comprendere i concetti avanzati di NLP.

1. Che cos'è NLP: una breve descrizione di NLP e delle sue applicazioni in campi come la traduzione automatica, l'analisi del sentimento e la generazione automatica del testo.

2. Python per NLP: una introduzione alle librerie di Python più utilizzate per il preprocessing dei dati di testo, come NLTK, Spacy, e Gensim.

3. Comprensione del testo: come l'NLP aiuta a comprendere il significato del testo, attraverso tecniche come il pos-tagging, l'analisi delle named entities e la classificazione del testo.

4. Strumenti di NLP: una panoramica dei vari strumenti e metodi utilizzati in NLP, come le word embeddings, la tokenizzazione, la pulizia del testo e la rimozione delle stop-word.

### 1. Introduzione a NLP

Natural Language Processing (NLP) è una branca dell'Intelligenza Artificiale che si concentra sulla comprensione e l'elaborazione del linguaggio umano. L'obiettivo principale dell'NLP è di sviluppare algoritmi e tecnologie che possano comprendere e analizzare il linguaggio umano, al fine di rendere possibile l'interazione umano-macchina più fluida e naturale.

NLP ha molte applicazioni in diversi campi, tra cui la traduzione automatica, l'analisi del sentimento e la generazione automatica del testo. La traduzione automatica utilizza l'NLP per tradurre il testo da una lingua a un'altra, mentre l'analisi del sentimento utilizza l'NLP per analizzare il tono e l'atteggiamento espresso in un testo. La generazione automatica del testo utilizza l'NLP per produrre testo coerente e significativo, a partire da una serie di prompt o di istruzioni.

L'NLP sfrutta tecniche di Deep Learning e di Machine Learning per analizzare e comprendere il linguaggio umano. Ad esempio, le reti neurali possono essere addestrate su grandi quantità di dati di testo per identificare le relazioni tra le parole e le frasi, mentre le tecniche di embedding delle parole possono essere utilizzate per rappresentare le parole in uno spazio numerico. Questo rende possibile l'analisi e la manipolazione automatica del testo in modo più preciso e veloce.

L'NLP sta diventando sempre più importante nell'era digitale, in cui il linguaggio naturale è utilizzato in molti modi, come le chatbot, le applicazioni di assistenza virtuale, i sistemi di ricerca e molto altro ancora. La capacità di comprendere e analizzare il linguaggio umano è diventata cruciale per sviluppare tecnologie che possano interagire con l'utente in modo più naturale e intuitivo.

In conclusione, l'NLP è una disciplina in rapida crescita che ha un impatto significativo su molte aree, dalla traduzione automatica alla generazione automatica del testo. Continua a svilupparsi e migliorare grazie alla combinazione di tecniche di Deep Learning e Machine Learning, e la sua importanza continuerà a crescere nei prossimi anni.

### 2. Python per NLP

Il Natural Language Processing (NLP) è un campo che si concentra sulla comprensione e sulla manipolazione del linguaggio umano da parte dei computer. In questo settore, Python è un linguaggio di programmazione molto popolare, grazie alla sua vasta gamma di librerie che supportano il NLP.

NLTK, ovvero Natural Language Toolkit, è una delle librerie di NLP più conosciute e utilizzate in Python. Offre una vasta gamma di strumenti per il preprocessing dei dati di testo, tra cui tokenizzazione, tagging delle parti del discorso, riconoscimento di entity named e molti altri. Questa libreria è un punto di partenza ideale per chi sta cercando di acquisire familiarità con i concetti di base del NLP.

Esempio dell'uso di NLTK:

```python
>>> import nltk
>>> sentence = """At eight o'clock on Thursday morning
... Arthur didn't feel very good."""
>>> tokens = nltk.word_tokenize(sentence)
>>> tokens
['At', 'eight', "o'clock", 'on', 'Thursday', 'morning',
'Arthur', 'did', "n't", 'feel', 'very', 'good', '.']
>>> tagged = nltk.pos_tag(tokens)
>>> tagged[0:6]
[('At', 'IN'), ('eight', 'CD'), ("o'clock", 'JJ'), ('on', 'IN'),
('Thursday', 'NNP'), ('morning', 'NN')]
```

Spacy è un'altra libreria di NLP in Python molto popolare. A differenza di NLTK, Spacy è più veloce e scalabile, e viene utilizzato principalmente per la tokenizzazione e il tagging delle parti del discorso. Offre inoltre una vasta gamma di modelli pre-addestrati che possono essere utilizzati per la riconoscimento di named entity, l'analisi del sentimento e molto altro.

Esempio dell'uso di Spacy:

```python

```

Gensim è un'altra libreria di NLP in Python che si concentra sulla modellizzazione del testo. Offre una vasta gamma di tecniche per la rappresentazione del testo, tra cui BOW (Bag of Words), TF-IDF (Term Frequency-Inverse Document Frequency) e word embeddings. Inoltre, Gensim include anche algoritmi avanzati per la modellizzazione del testo, come il Latent Dirichlet Allocation (LDA) e il Latent Semantic Analysis (LSA).

In conclusione, NLTK, Spacy e Gensim sono solo alcune delle librerie di NLP in Python più utilizzate. Ognuna di queste librerie offre una vasta gamma di strumenti e tecniche per il preprocessing dei dati di testo, e sono fondamentali per chiunque desideri sviluppare applicazioni di NLP con Python.

### 3. Comprensione del testo

La comprensione del testo è una delle sfide più importanti nell'ambito del Natural Language Processing (NLP). L'obiettivo principale è quello di comprendere il significato del testo e trasformarlo in informazioni strutturate che possono essere utilizzate da computer e altre applicazioni.

Una delle tecniche più utilizzate nella comprensione del testo è il pos-tagging, che consiste nell'identificare e marcare la posizione di ogni parola all'interno di un testo. Questo aiuta a comprendere il significato del testo e a identificare la funzione di ogni parola all'interno di una frase.

L'analisi delle named entities è un'altra tecnica importante che aiuta a comprendere il significato del testo. Questa tecnica identifica e classifica le informazioni importanti all'interno di un testo, come nomi di persone, luoghi, organizzazioni e altri elementi. Queste informazioni possono essere utilizzate per classificare il testo e comprenderne meglio il significato.

La classificazione del testo è un'altra tecnica importante utilizzata per comprendere il significato del testo. Questa tecnica utilizza algoritmi di machine learning per analizzare il testo e classificarlo in categorie predefinite, come ad esempio negativo o positivo per l'analisi del sentimento.

In sintesi, l'NLP aiuta a comprendere il significato del testo attraverso tecniche come il pos-tagging, l'analisi delle named entities e la classificazione del testo. Queste tecniche sono utilizzate in molti campi, come la traduzione automatica, l'analisi del sentimento e la generazione automatica del testo.

### Strumenti dell'NLP

Il Natural Language Processing (NLP) è una branca dell'Intelligenza Artificiale che si concentra sulla comprensione e l'elaborazione del linguaggio umano. I suoi obiettivi sono quelli di rendere possibile la comunicazione tra macchine e persone, nonché di analizzare e comprendere il significato del testo.

Per raggiungere questi obiettivi, gli sviluppatori di NLP utilizzano vari strumenti e metodi, tra cui la tokenizzazione, la pulizia del testo, la rimozione delle stop-word e le word embeddings. La tokenizzazione consiste nel dividere un testo in singole parole o frasi, mentre la pulizia del testo mira a rimuovere eventuali errori o informazioni inutili. La rimozione delle stop-word, invece, si occupa di eliminare parole comuni come "e", "di", "il", che non apportano alcun significato al testo.

Le word embeddings sono un altro importante strumento utilizzato in NLP. Esse rappresentano le parole come vettori di numeri, che ne descrivono il significato in base al contesto in cui vengono utilizzate. Questa rappresentazione numerica delle parole permette alle macchine di elaborare e comprendere il significato delle parole in modo più efficiente rispetto a una rappresentazione basata su stringhe di testo.

In sintesi, i vari strumenti e metodi utilizzati in NLP sono fondamentali per l'analisi e la comprensione del testo. Essi permettono di realizzare applicazioni avanzate, come la traduzione automatica, l'analisi del sentimento e la generazione automatica del testo, e rappresentano una parte essenziale per lo sviluppo di sistemi di intelligenza artificiale in grado di comprendere e utilizzare il linguaggio umano.

## Preparazione dei dati: tokenizzazione, pulizia del testo, eliminazione delle stop-word

Il preprocessing dei dati è un passaggio cruciale per il successo dei 
modelli di NLP, in quanto aiuta a preparare i dati in modo che siano più
 facili da gestire e analizzare.

1. Tokenizzazione: come suddividere il testo in unità significative, come le parole o le frasi.

2. Pulizia del testo: come rimuovere elementi non necessari dal testo, come la punteggiatura, i numeri e le stringhe vuote.

3. Rimozione delle stop-word: come identificare e rimuovere le parole di stop-word, come "the" o "and", che non apportano informazioni significative al testo.

4. Stemming e Lemmatizzazione: come trasformare le parole in forme più generali, aiutando a ridurre la dimensione del vocabolario e migliorare la rappresentazione del testo.
   
   ### Introduzione alla preparazione del testo
   
   La preparazione dei dati è un passo cruciale per ottenere buoni risultati in molti task di NLP. La tokenizzazione consiste nel suddividere il testo in unità significative, come le parole o le frasi. Questo passo è importante perché permette di lavorare con singole parole invece che con una stringa lunga e complessa. La pulizia del testo riguarda la rimozione di elementi non necessari dal testo, come la punteggiatura, i numeri e le stringhe vuote. Questo passo è importante perché questi elementi possono causare problemi nella rappresentazione del testo e nelle successive analisi.
   
   La rimozione delle stop-word è un altro passo importante nella preparazione dei dati. Le stop-word sono parole comuni come "the" o "and" che non apportano informazioni significative al testo. Queste parole possono essere rimosse per ridurre la dimensione del vocabolario e migliorare la rappresentazione del testo. Questi passaggi di pulizia e rimozione sono fondamentali per ottenere buoni risultati in molti task di NLP, come la classificazione del testo o la clustering semantico.
   
   In Python, esistono molte librerie NLP che possono aiutare con la tokenizzazione, la pulizia del testo e la rimozione delle stop-word, tra cui NLTK e spacy. Ad esempio, con spacy è possibile eseguire la tokenizzazione utilizzando la proprietà "doc" e la pulizia del testo utilizzando la proprietà "text". Inoltre, spacy fornisce anche una lista di stop-word che possono essere utilizzate per la rimozione delle stop-word. Anche NLTK ha i suoi metodi e funzioni per raggiungere lo stesso scopo, come vedremo.
   
   In definitiva, la preparazione dei dati è un passo cruciale per ottenere buoni risultati in molti task di NLP e aiuta a migliorare la rappresentazione del testo. La tokenizzazione, la pulizia del testo e la rimozione delle stop-word sono tutti passaggi importanti che vanno eseguiti prima di iniziare qualsiasi analisi.
   
   ### 1. Tokenizzazione
   
   La tokenizzazione è un processo fondamentale nel Natural Language Processing (NLP) che consiste nella suddivisione del testo in unità significative, come parole o frasi. Queste unità sono chiamate token e rappresentano la base per la maggior parte delle operazioni di elaborazione del linguaggio naturale. La tokenizzazione è importante perché permette di identificare facilmente il significato del testo e di elaborarlo in modo più efficiente.
   
   Esistono diversi metodi di tokenizzazione, ognuno dei quali può essere utilizzato a seconda delle esigenze specifiche del progetto. Ad esempio, la tokenizzazione basata su parole divide il testo in parole singole, mentre la tokenizzazione basata su frasi divide il testo in frasi complete. La scelta del metodo dipende dal tipo di informazione che si vuole estrarre dal testo e dall'obiettivo specifico del progetto.
   
   In NLP, la tokenizzazione è spesso seguita da altri passaggi di pre-elaborazione, come la pulizia del testo e la rimozione delle stop-word, che aiutano a rimuovere eventuali elementi non necessari dal testo e a rendere i token più significativi per l'analisi.
   
   In sintesi, la tokenizzazione è un passo cruciale nella preparazione dei dati di testo per l'elaborazione del linguaggio naturale. La scelta del metodo più adatto e la combinazione con altri passaggi di pre-elaborazione sono fondamentali per garantire che il testo possa essere elaborato in modo efficiente e che l'analisi sia più precisa e significativa.
   
   ```python
   import nltk
   nltk.download('punkt')
   from nltk.tokenize import word_tokenize
   
   text = "Questo è un esempio di testo che voglio tokenizzare."
   tokens = word_tokenize(text)
   print(tokens)
   ```
   
   In questo codice, importiamo la libreria `nltk` e scarichiamo il modulo `punkt`, che viene utilizzato per la tokenizzazione delle parole. Dopodiché, utilizziamo la funzione `word_tokenize` per suddividere il testo in parole e stampiamo il risultato. L'output di questo codice sarà:
   
   ```shell
   ['Questo', 'è', 'un', 'esempio', 'di', 'testo', 'che', 'voglio', 'tokenizzare', '.']
   ```

Mentre NLTK ha una funzione interna di download, per Spacy dobbiamo usare la linea di comando per installare imoduli di cui abbiamo bisogno. Vediamo quindi adesso come scaricare il modulo in lingua italiana e usarlo per un esempio di tokenizzazione usando Spacy:

```shell
python -m spacy download it_core_news_sm
```

Adesso possiamo vedere come tokenizzare con Spacy direttamente in italiano:

```python
import spacy

# Carica il modello in lingua italiana
nlp = spacy.load("it_core_news_sm")

# Definisci il testo da tokenizzare
testo = "Questo è un esempio di tokenizzazione di un testo in Python con spaCy."

# Crea un oggetto Doc dal testo
doc = nlp(testo)

# Stampa ogni token del testo
for token in doc:
    print(token.text)
```

Il codice carica il modello in lingua italiana, definisce un testo da tokenizzare e crea un oggetto `Doc` dal testo utilizzando la funzione `nlp`. Quindi, il codice stampa ogni token del testo, utilizzando il campo `text` di ogni token.

### 2. Pulizia del testo

La pulizia del testo è un passo importante nell'elaborazione del linguaggio naturale (NLP) che mira a rimuovere gli elementi non necessari dal testo, come la punteggiatura, i numeri e le stringhe vuote, in modo che il testo sia pronto per ulteriori elaborazioni. Questo processo aiuta a standardizzare il testo e a renderlo più facile da gestire per i modelli NLP.

In Python, la pulizia del testo può essere effettuata utilizzando diverse librerie NLP, come NLTK e Spacy, che forniscono funzioni già implementate per la pulizia del testo. Ad esempio, si può utilizzare la funzione `word_tokenize` di NLTK per dividere il testo in parole e la funzione `sent_tokenize` per suddividere il testo in frasi.

Anche senza usare una libreria specifica per NLP, è possibile utilizzare espressioni regolari (regex) per rimuovere caratteri indesiderati dal testo. Ad esempio, si può utilizzare la funzione `re.sub` di Python per sostituire i numeri con una stringa vuota e la punteggiatura con uno spazio:

```python
import re

text = "Questo è un esempio di testo. 123, con numeri e punteggiatura!"

# Rimuovi numeri
text = re.sub(r'\d+', '', text)
print(text)
# Output: Questo è un esempio di testo. , con numeri e punteggiatura!

# Rimuovi punteggiatura
text = re.sub(r'[^\w\s]', ' ', text)
print(text)
# Output: Questo è un esempio di testo  con numeri e punteggiatura
# Rimuovi spazi bianchi

text = re.sub(r'\s+', ' ', text)
print(text)
# Output: Questo è un esempio di testo con numeri e punteggiatura 
```

Ovviamnete, è possibile rimuovere le stop-word, ovvero le parole comuni che non apportano significato al testo, utilizzando la funzione `stopwords.words` di NLTK.

```python
import nltk
from nltk.corpus import stopwords

# Scaricare la lista delle stop-word
nltk.download('stopwords')

# Estrarre la lista delle stop-word in inglese
stop_words = set(stopwords.words('italian'))

# Esempio di testo da pulire
text = "Questo è un esempio di pulizia del testo usando la libreria NLTK in Python. Rimuove le stop words dal testo."

# Dividi il testo in parole
words = nltk.word_tokenize(text)

# Rimuovi le stop-word dal testo
filtered_words = [word for word in words if word.lower() not in stop_words]

# Unisci le parole pulite in un testo
cleaned_text = " ".join(filtered_words)

print("Testo originale:", text)
print("Testo pulito:", cleaned_text)
```

In questo esempio, il testo viene diviso in parole utilizzando la 
funzione word_tokenize, quindi le stop-word vengono rimosse dal testo 
utilizzando una list comprehension. Infine, le parole pulite vengono 
unite di nuovo in un testo utilizzando il metodo join.

Vedremo la rimozione delle stopwords più in dettaglio nella prossima sezione.

Anche SpaCy ha delle funzioni per pulire il testo:

```python
import spacy

# Carica il modello di lingua in spacy
nlp = spacy.load("it_core_news_sm")

# Definisci il testo da pulire
testo = "Questo è un esempio di testo con numeri, punteggiatura e caratteri non alfabetici. 1234567890"

# Crea un oggetto Doc da una stringa di testo
doc = nlp(testo)

# Itera su ogni token nel documento
tokens_puliti = [token.text for token in doc if not token.is_punct | token.is_space | token.is_digit ]

# Unisci i token puliti in una stringa di testo
testo_pulito = " ".join(tokens_puliti)

# Stampa il testo pulito
print(testo_pulito)
```

In questo esempio, utilizziamo la funzione `nlp()` per creare un oggetto Doc da una stringa di testo. Successivamente, utilizziamo il metodo `is_punct`, `is_space`, e`is_digit` per identificare e filtrare i token che rappresentano punteggiatura, spazi, numeri o stopword. Infine, utilizziamo la funzione `join()` per unire i token puliti in una stringa di testo.

La pulizia del testo è un passo cruciale per ottenere un testo standardizzato e pronto per ulteriori elaborazioni in NLP. Utilizzando le funzioni delle librerie NLP o le espressioni regolari, è possibile effettuare facilmente questo processo in Python.

### 3. Rimozione delle stop-word

La rimozione delle stop-word è una fase importante nel preprocessing dei dati di testo in NLP. Le stop-word sono parole che vengono utilizzate frequentemente nel linguaggio ma che non apportano informazioni significative al testo. Queste parole sono spesso definite in una lista precompilata e possono essere facilmente identificate e rimosse.

La rimozione delle stop-word è utile perché riduce la quantità di dati che devono essere elaborati dai modelli di NLP, migliorando così la loro efficienza. Inoltre, eliminando le stop-word, i modelli possono concentrarsi sulle parole che hanno un impatto significativo sul significato del testo.

In Python, la libreria NLTK fornisce una funzione stopwords.words() che 
può essere utilizzata per identificare e rimuovere le stop-word. Questa 
funzione accetta come argomento la lingua per la quale si desidera 
ottenere la lista delle stop-word. Ad esempio, per ottenere la lista 
delle stop-word in italiano, è possibile utilizzare il codice seguente:

```python
import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')

stop_words = set(stopwords.words('italian'))

def remove_stop_words(text):
    words = nltk.word_tokenize(text)
    words = [word.lower() for word in words if word.isalpha()]
    words = [word for word in words if word not in stop_words]
    return words

text = "Questo è un esempio di pulizia del testo usando la libreria NLTK in Python. Rimuove le stop words dal testo."

print(remove_stop_words(text))
```

In questo codice, la funzione remove_stop_words() accetta come argomento  un testo e restituisce una lista di parole in cui sono state eliminate le stop-word. La funzione prima utilizza word_tokenize() per suddividere il testo in parole, quindi rimuove tutte le parole che non sono alfabetiche e infine rimuove tutte le parole presenti nella lista delle stop-word.

Come si evince dal codice, nltk non ha varie funzioni per l'italiano, ma ha un elenco di stopwords specifico per la lingua.

Se volessimo usare Spacy per lo stesso compito, dovremmo solo aggiungere la funzione `is_stop`all'esempio di pulizia del testo, per avere un testo pulito e senza stopwords:

```python
import spacy

# Carica il modello di lingua in spacy
nlp = spacy.load("it_core_news_sm")

# Definisci il testo da pulire
testo = "Questo è un esempio di testo con numeri, punteggiatura e caratteri non alfabetici. 1234567890"

# Crea un oggetto Doc da una stringa di testo
doc = nlp(testo)

# Itera su ogni token nel documento
tokens_puliti = [token.text for token in doc if not token.is_punct | token.is_space | token.is_digit | token.is_stop]

# Unisci i token puliti in una stringa di testo
testo_pulito = " ".join(tokens_puliti)

# Stampa il testo pulito
print(testo_pulito)
```

In questo esempio, utilizziamo la funzione `nlp()` per creare un oggetto Doc da una stringa di testo. Successivamente, utilizziamo il metodo `is_punct`, `is_space`, `is_digit` e `is_stop` per identificare e filtrare i token che rappresentano punteggiatura, spazi, numeri o stopword. Infine, utilizziamo la funzione `join()` per unire i token puliti in una stringa di testo.

### 4. Stemming e Lemmatizzazione

La Stemming e la Lemmatizzazione sono tecniche importanti utilizzate in NLP per trasformare le parole in forme più generali. Il loro scopo è di ridurre la dimensione del vocabolario e migliorare la rappresentazione del testo.

La Stemming è un processo che consiste nel rimuovere la parte finale di una parola, come suffissi o prefissi, per ottenere una forma più semplice e generale della parola stessa. Ad esempio, la parola "correndo" verrebbe trasformata in "corre". Questo processo è utile perché molte parole derivate hanno significati simili e possono essere trattate come la stessa parola base.

La Lemmatizzazione, d'altra parte, è un processo più sofisticato che considera il contesto semantico delle parole. Invece di semplicemente rimuovere la parte finale di una parola, la Lemmatizzazione la trasforma in una forma canonica, che può essere una parola di base o una forma del dizionario. Ad esempio, la parola "correndo" verrebbe trasformata in "correre".

Entrambi questi processi sono importanti perché aiutano a ridurre la dimensionalità del testo, facilitando così l'analisi. Inoltre, possono aiutare a migliorare la precisione dei modelli di NLP, come la classificazione del testo e l'analisi del sentimento, poiché le parole sono rappresentate in forme più generali e coerenti.

In Python, le librerie di NLP come NLTK e Spacy forniscono funzioni per la Stemming e la Lemmatizzazione. Ad esempio, in NLTK, si può utilizzare la funzione `PorterStemmer` per la Stemming e la funzione `WordNetLemmatizer` per la Lemmatizzazione.

L'algoritmo di stemming di Porter è uno dei più utilizzati in NLP ed è 
implementato nella libreria NLTK. Ecco un esempio di come utilizzare il 
modulo PorterStemmer per eseguire stemming su un testo:

```python
import nltk
from nltk.stem import PorterStemmer

# Crea un'istanza di PorterStemmer
stemmer = PorterStemmer()

# Definisci il testo da stemmare
testo = "La corsa del cane era molto veloce. Il gatto correva dietro di lui."

# Tokenizza il testo
parole = nltk.word_tokenize(testo)

# Stemma ogni parola del testo
parole_stemmate = [stemmer.stem(parola) for parola in parole]

# Stampa le parole stemmate
print(parole_stemmate)
```

Nel codice, viene prima creata un'istanza di `PorterStemmer`.
 Poi, viene definito il testo che verrà stemmato, che viene 
successivamente tokenizzato in parole individuali. Infine, viene 
effettuato il stemming di ogni parola utilizzando il metodo `stem` dell'istanza di `PorterStemmer`, e i risultati vengono stampati.

L'output sarà: `['la', 'cors', 'del', 'cane', 'era', 'molto', 'veloc', '.', 'il', 'gatto', 'correv', 'dietro', 'di', 'lui', '.']`

Purtroppo `NLTK` non ha un modulo specifico per l'italiano. In generale per la lemmatizzazione questo non è un grosso problema però.

 `Spacy` invece ha unmodulo specifico per l'italianocome abbiamo visto:

```python
import spacy

# Carica il modello di lingua italiana
nlp = spacy.load("it_core_news_sm")

# Definisci il testo da stemmare
testo = "La corsa del cane era molto veloce. Il gatto correva dietro di lui."

# Processa il testo con spacy
doc = nlp(testo)

# Stemma ogni parola del testo
parole_stemmate = [token.text.lower() for token in doc if not token.is_punct | token.is_space]

# Stampa le parole stemmate
print(parole_stemmate)
```

In questo esempio, viene utilizzato il modulo `spacy.lang.en` per processare il testo, quindi ogni token viene convertito in minuscolo e le parole che sono punteggiatura o spazi vuoti vengono ignorate.

```shell
['la', 'corsa', 'del', 'cane', 'era', 'molto', 'veloce', 'il', 'gatto', 'correva', 'dietro', 'di', 'lui']
```

La lemmatizzazione  consiste nel trasformare le parole in forme più generali che hanno un significato simile, come "correre" invece di "corse". Il risultato 
finale è una lista di parole lemmatizzate che possono essere utilizzate 
per ulteriori analisi.

L'esempio seguente mostra come utilizzare la funzione `WordNetLemmatizer` di NLTK per eseguire la lemmatizzazione di un testo in Python:

```python
import nltk
from nltk.stem import WordNetLemmatizer

# Scarica WordNet
nltk.download('wordnet')

# Crea un'istanza di WordNetLemmatizer
lemmatizer = WordNetLemmatizer()

# Definisci il testo da lemmatizzare
text = "The dog's run was very fast. The cat ran after it."

# Tokenizza il testo
parole = nltk.word_tokenize(text)

# Lemmatizza ogni parola del testo
parole_lemmatizzate = [lemmatizer.lemmatize(parola) for parola in parole]

# Stampa le parole lemmatizzate
print(parole_lemmatizzate)
```

IIn questo esempio, la funzione `WordNetLemmatizer` viene 
utilizzata per lemmatizzare ogni parola del testo. 

L'output è:

```shell
['The', 'dog', "'s", 'run', 'wa', 'very', 'fast', '.', 'The', 'cat', 'ran', 'after', 'it', '.']
```

Se il processo di stemming fatto da NLTK senza un modulo specifico può andare bene per l'italiano, per la lemmizzazione è molto meglio usare un modulo specifico per la lingua. Vediamo adesso come lemmatizzare in Spacy:

```python
import spacy

# Carica il modello di Spacy
nlp = spacy.load("it_core_news_sm")

# Definisci il testo da lemmatizzare
testo = "La corsa del cane era molto veloce. Il gatto correva dietro di lui."

# Crea un oggetto di tipo Doc dal testo
doc = nlp(testo)

# Lemmatizza ogni token del documento
lemmi = [token.lemma_ for token in doc]

# Stampa i lemmi
print(lemmi)
```

Questo codice carica il modello di lingua italiana "it_core_news_sm" di Spacy e utilizza la funzione `nlp` per creare un oggetto di tipo `Doc` dal testo. Quindi, scorre ogni token del documento e ottiene il lemma di ogni token utilizzando la proprietà `lemma_`. Infine, stampa i lemmi ottenuti.

```shell
['il', 'corsa', 'di il', 'cane', 'essere', 'molto', 'veloce', '.', 'il', 'gatto', 'correre', 'dietro', 'di', 'lui', '.']
```

Come possiamo vedere dal lemma "di il", magari sarebbe opportuno pulire le stopwords prima di passare alla lemmatizzazione.

In sintesi, la Stemming e la Lemmatizzazione sono tecniche importanti per la rappresentazione del testo in NLP e sono facilmente implementabili con le librerie di Python per NLP.

## Representation del testo: BOW, TF-IDF, word embeddings

La scelta della rappresentazione del testo giusta dipende 
dall'applicazione specifica e dai requisiti del modello, quindi è 
importante che gli studenti comprendano i vantaggi e gli svantaggi di 
ciascuno di questi modelli.

1. BOW (Bag of Words): come rappresentare il testo come un vettore di conteggi di parole, trascurando l'ordine e la struttura delle parole all'interno del testo.

2. TF-IDF (Term Frequency-Inverse Document Frequency): come pesare le parole in base alla loro importanza nell'intero corpus di documenti, per migliorare la rappresentazione del testo.

3. Word Embeddings: come rappresentare le parole come vettori multi-dimensionali, che catturano le relazioni semantiche tra le parole. Parleremo di word2vec, GloVe, e altri modelli di word embedding.

### Introduzione alla rappresentazione del testo

La rappresentazione del testo è uno dei compiti fondamentali nel Natural Language Processing (NLP) e consiste nel convertire un documento di testo in una forma che possa essere utilizzata per l'analisi automatica. Ci sono tre rappresentazioni di testo comunemente utilizzate in NLP: Bag of Words (BOW), Term Frequency-Inverse Document Frequency (TF-IDF) e word embeddings.

Bag of Words (BOW) è una rappresentazione semplice del testo che consiste nella conteggio delle parole in un documento. Questo modello viene spesso utilizzato come punto di partenza per altri modelli più complessi. Tuttavia, questo modello non tiene conto dell'ordine delle parole o della loro frequenza relativa in un documento.

Term Frequency-Inverse Document Frequency (TF-IDF) è un modello più avanzato che tiene conto sia della frequenza delle parole che della loro importanza relativa a livello di documento. Questo modello utilizza una formula che calcola la frequenza delle parole in un documento e la divide per la frequenza di una parola in tutti i documenti. Questo fornisce una rappresentazione più precisa del testo rispetto a BOW.

Word embeddings sono una rappresentazione densa e continuamente allenata delle parole in un modello di apprendimento automatico. Questo modello rappresenta ogni parola come un vettore di numeri, dove ogni numero rappresenta una proprietà specifica della parola. Questa rappresentazione è molto utile per l'analisi del testo poiché tiene conto del significato semantico delle parole.

In conclusione, la scelta della rappresentazione di testo più appropriata dipenderà dall'obiettivo specifico del progetto di NLP e dalle esigenze di analisi. Sia BOW che TF-IDF sono modelli semplici ma utili, mentre gli embeddings delle parole offrono una rappresentazione più avanzata del testo con una maggiore considerazione del significato semantico delle parole.

### 1. Rappresentazione Bag of Words (BOW)

La rappresentazione del testo come un vettore di conteggi di parole è nota come BOW (Bag of Words). Questa rappresentazione è una delle forme più semplici e tradizionali di rappresentazione del testo in NLP. Il BOW trascura l'ordine e la struttura delle parole all'interno del testo e si concentra solo sul conteggio delle parole.

La prima fase per creare una rappresentazione BOW è quella di tokenizzare il testo in parole individuali. Successivamente, vengono create le occorrenze di ogni parola, che sono poi utilizzate per creare un dizionario di parole uniche. Queste parole uniche sono utilizzate come features per rappresentare il testo. Ogni documento è quindi rappresentato come un vettore di dimensione uguale al numero di parole uniche nel dizionario, con ogni elemento del vettore che rappresenta il numero di occorrenze di una parola specifica in quel documento.

Il BOW è una rappresentazione molto utile per molti task di NLP, come la classificazione del testo, l'analisi del sentimento e la clusterizzazione. Tuttavia, questa rappresentazione ha alcuni limiti, come l'ignoranza del contesto semantico delle parole e l'impossibilità di rappresentare la relazione tra le parole. Per superare questi limiti, sono stati sviluppati altri metodi di rappresentazione del testo, come le word embeddings e la rappresentazione del testo come grafo.

In sintesi, il BOW è una rappresentazione molto semplice e tradizionale del testo in NLP che è utile per molti task, ma che presenta alcuni limiti. Questi limiti possono essere superati utilizzando metodi più avanzati di rappresentazione del testo.

Per creare una rappresentazione BOW possiamo usare le funzioni della libreria scikit-learn per rappresentare un testo come un vettore BOW. Per prima cosa, bisogna installare scikit-learn:

```shell
pip install scikit-learn
```

Adesso passiamo al codice necessario per la rappresentazione BOW:

```python
from sklearn.feature_extraction.text import CountVectorizer

# Definire i documenti
documenti = ["Il gatto corre velocemente.", "Il cane è veloce.", "Il gatto è molto veloce."]

# Creare un'istanza di CountVectorizer
vectorizer = CountVectorizer()

# Trasformare i documenti in una matrice BOW
bow = vectorizer.fit_transform(documenti)

# Stampa le features (parole) presenti nel vocabolario
print(vectorizer.get_feature_names_out())

# Stampa la matrice BOW
print(bow.toarray())
```

Come prima cosa  stampiamo le features (le parole contate) e poi il BOW. Il risultato del BOW sarà una matrice che rappresenta i documenti come vettori 
di conteggi di parole. Ogni riga rappresenta un documento e ogni colonna
 rappresenta una parola presente nel vocabolario. Il valore in ogni 
cella è il conteggio della parola corrispondente nel documento 
corrispondente.

```shell
['cane' 'corre' 'gatto' 'il' 'molto' 'veloce' 'velocemente']
[[0 1 1 1 0 0 1]
 [1 0 0 1 0 1 0]
 [0 0 1 1 1 1 0]]
```

Ovviamente, è possibile utilizzare la classe `CountVectorizer` di scikit-learn in combinazione con le funzionalità di NLTK o Spacy per la tokenizzazione, la pulizia del testo, la rimozione delle stop-word per poi costruire un modello BOW a partire da un corpus di testo pulito.

Nell'esempio precedente vediamo che c'è la feature "il" che non ci interessa. 

Puliamo quindi il testo prima di applicare il BOW; nel prossimo esempio, per variare, non usiamo scikit-learn, ma `Counter` dalla collezione standard di Python, e SpaCy per la pulizia del testo:

```python
import spacy
import collections

nlp = spacy.load("it_core_news_sm")

# Definisci il testo da pulire e rappresentare come BOW
documenti = ["Il gatto corre velocemente.", "Il cane è veloce.", "Il gatto è molto veloce."]
testo = " ".join(documenti)
# Pulisci il testo utilizzando Spacy
doc = nlp(testo)

# Rimuovi i token che non sono parole o che sono stop-word e lemmatizza
lemmi = [token.lemma_ for token in doc if token.is_alpha and not token.is_stop]

# Crea una rappresentazione BOW del testo
bow = collections.Counter(lemmi)

# Stampa il BOW
print(bow)
```

L'output sarà:

```shell
Counter({'gatto': 2, 'veloce': 2, 'corre': 1, 'velocemente': 1, 'cane': 1})
```

### 2. Rappresentazione Term Frequency-Inverse Document Frequency (TF-TDF)

Il TF-IDF (Term Frequency-Inverse Document Frequency) è una tecnica di pesatura delle parole che aiuta a rappresentare il testo in modo più significativo. Questo metodo di rappresentazione del testo è spesso utilizzato in molte applicazioni NLP, come la classificazione del testo, la ricerca di informazioni e la clustering di documenti.

Il TF-IDF si basa su due concetti fondamentali: la frequenza del termine (TF) e la frequenza inversa del documento (IDF). La frequenza del termine misura quante volte una parola appare in un documento. La frequenza inversa del documento misura quanto è raro un termine all'interno di un corpus di documenti.

In pratica, le parole che compaiono frequentemente in un documento ma sono rare all'interno di un corpus di documenti hanno un peso più alto. Questo perché queste parole sono considerate più informative rispetto a quelle che compaiono frequentemente in tutti i documenti.

La rappresentazione del testo come vettore di TF-IDF può essere ottenuta utilizzando librerie NLP come scikit-learn o gensim. Queste librerie forniscono anche funzioni per calcolare automaticamente il peso TF-IDF per ogni termine nel testo.

In conclusione, la rappresentazione del testo tramite TF-IDF è un metodo utile per migliorare la rappresentazione del testo. Aiuta a evidenziare le parole più informative all'interno del testo e può essere utilizzato in molte applicazioni NLP.

Passiamo ad un esempio di rappresentazione del testo come vettore di TF-IDF. Abbiamo bisogno di installare la libreria Pandas per lavorare sui dataframe:

```shell
pip install pandas
```

Adesso passiamo al codice necessario:

```python
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer

# Crea un dataset di esempio
documenti = ['Il gatto correva dietro il cane', 
             'Il cane è veloce', 
             'Il gatto è pigro']

# Crea un dataframe
df = pd.DataFrame({'documenti': documenti})

# Crea un oggetto TfidfVectorizer
tfidf = TfidfVectorizer()

# Calcola i valori di TF-IDF per ciascun documento
matrice_tfidf = tfidf.fit_transform(df['documenti'])

# Raccogli le features (i nomi analizzati)
features = tfidf.get_feature_names_out()

# Crea un DataFrame per visualizzare i pesi TF-IDF
df_tfidf = pd.DataFrame(matrice_tfidf.todense(), columns = features)

# Stampa le prime 5 righe del DataFrame
print(df_tfidf.head())
```

Questo codice crea un dataset di esempio che rappresenta tre documenti. Poi, crea un oggetto `TfidfVectorizer` e utilizza il metodo `fit_transform` per calcolare i valori di TF-IDF per ogni documento. Infine, mostra i valori di TF-IDF come una matrice, mostrando come nome delle colonne le features (cioè le parole) analizzate.

L'output è il seguente:

```shell
       cane   correva    dietro     gatto        il     pigro    veloce
0  0.356457  0.468699  0.468699  0.356457  0.553642  0.000000  0.000000
1  0.547832  0.000000  0.000000  0.000000  0.425441  0.000000  0.720333
2  0.000000  0.000000  0.000000  0.547832  0.425441  0.720333  0.000000
```

### 3.  Word Embedding

Il processo di rappresentare le parole come vettori multi-dimensionali è noto come word embedding. In questo processo, ogni parola viene associata a un vettore numerico che cattura le sue proprietà semantiche e relazioni con altre parole. Questa rappresentazione è utile per molti compiti di NLP, come la classificazione del testo, la traduzione automatica e la generazione di testo.

Uno dei modelli più comuni di word embedding è word2vec, sviluppato da Google nel 2013. Questo modello allena una rete neurale su grandi quantità di testo per imparare una rappresentazione vectoriale delle parole. Questa rappresentazione viene appresa utilizzando una coppia di parole vicine nella frase o nella pagina, il che significa che le parole simili tendono ad avere vettori simili.

Un altro modello di word embedding molto popolare è GloVe (Global Vectors for Word Representation), sviluppato dalla Stanford University. Questo modello utilizza una tecnica di factorizzazione delle matrici per apprendere la rappresentazione vettoriale delle parole a partire dalla loro frequenza co-occorrenza in un grande corpus di testo. Questo modello cattura le proprietà semantiche e sintattiche delle parole, rendendolo utile per molte applicazioni di NLP.

Oltre a word2vec e GloVe, ci sono anche altri modelli di word embedding, come ELMo, BERT, GPT-3, che hanno raggiunto risultati eccezionali in molti compiti NLP. Questi modelli utilizzano tecniche di deep learning avanzate per apprendere rappresentazioni vettoriali più sofisticate delle parole, che catturano molte più informazioni sul loro significato e sulle loro relazioni.

In sintesi, i word embeddings sono un'importante tecnologia di NLP che permette di rappresentare le parole come vettori multi-dimensionali, catturando le loro relazioni semantiche e le loro proprietà linguistiche. Questi modelli sono stati utilizzati con successo in molte applicazioni di NLP e continuano ad essere al centro della ricerca in questo campo.

Un esempio di word embedding, usando la libreria gensim che ha una funzione per `Word2Vec`; innanzitutto installiamo la libreria:

```shell
pip install gensim
```

Passiamo al codice:

```python
import gensim

# Definisci il testo
testo = "Questa è una prova di word embedding. Stiamo cercando di convertire le parole in vettori di significato."

# Tokenizza il testo in parole
parole = testo.split()

# Crea un modello word2vec
modello = gensim.models.Word2Vec([parole], min_count=1, vector_size=100)

# Accesso alla rappresentazione di una parola come vettore
vettore = modello.wv['parole']

# Stampa la rappresentazione del vettore
print(vettore)

# Ottieni le 5 parole più simili
simili = modello.wv.most_similar('parole', topn=5)

# Stampa le 5 parole più simili
print(simili)
```

In questo esempio, stiamo utilizzando la funzione `Word2Vec` di Gensim per creare un modello word2vec che rappresenti le parole del nostro testo come vettori. Il parametro `min_count` specifica il numero minimo di volte che una parola deve apparire nel testo per essere inclusa nel modello. Il parametro `size` specifica la dimensione del vettore per ogni parola. In questo caso, 
stiamo utilizzando una dimensione di 100. Infine, accediamo alla 
rappresentazione di una parola specifica come vettore utilizzando la 
notazione `modello.wv['parole']`. Questo vettore rappresenta 
la parola come un insieme di valori numerici che catturano il 
significato e le relazioni semantiche con le altre parole.

Dopo aver stampato il vettore di relazioni, cerchiamo e stampiamo le 5 parole più simili a "parole" con `modello.wv.most_similar('parole', topn=5)`.

Se vogliamo creare un `word2vec` e concentrarci solo sulla similarità, in gensim possiamo usare questa strategia:

```python
import gensim
from gensim.models import Word2Vec

# Crea un modello di word2vec
model = Word2Vec([['cane', 'gatto', 'uomo', 'donna'], ['casa', 'edificio', 'strada', 'città']], min_count=1)

# Visualizza le parole più simili a 'cane'
parole_simili = model.wv.most_similar(positive=['cane'], topn=5)
print(parole_simili)
```

Stranamente, l'output ci indica `città` come più vicino a `cane`che `gatto`, per esempio.

```shell
[('città', 0.09291721880435944), ('gatto', 0.004842504858970642), ('edificio', -0.002754013054072857), ('casa', -0.013679765164852142), ('donna', -0.028491046279668808)]
```

Questo non deve stupirci, infatti questi modelli preconfezionati sono pensati quasi esclusivamente per la lingua inglese.

Per quanto riguarda altri modelli, come è facile capire, lavorano quasi tutti esclusivamente per la lingua inglese. Ce ne sono vari in lingua italiana, uno dei quali è proprio il `it_core_news_sm` che abbiamo scaricato per i compiti di pulizia del testo in Spacy.

Vediamo come usarlo adesso per il word embedding di frasi in italiano:

```python
import spacy

# Carica un modello di lingua pre-addestrato in spacy
nlp = spacy.load("it_core_news_sm")

# Definisci il testo
testo = "La mia giornata è stata molto intensa. Ho dovuto lavorare molto al computer."

# Analizza il testo usando il modello di lingua pre-addestrato
doc = nlp(testo)

# Stampa le rappresentazioni di word embedding delle parole
for token in doc:
    print(token.text, token.vector[:5])

# Stampa il vettore per la parola in posizione 2, "giornata"
print(doc[2].vector)
```

In questo esempio, il modello pre-addestrato `it_core_news_sm` viene caricato usando `spacy.load()` e il testo viene analizzato usando la funzione `nlp()`. Quindi, viene stampato il testo di ogni parola e i primi 5 elementi del vettore di word embedding associato a ogni parola.

Infine, stampiamo il vettore per la parola in posizione 2, cioè "giornata" (ricordando che in Python la prima posizione di un array è la posizione `0`).

## Classificazione del testo: Naive Bayes, SVM, RNN

Ogni modello di classificazione del testo ha i suoi vantaggi e svantaggi, e la scelta del modello giusto dipende dalle esigenze specifiche dell'applicazione e dal tipo di dati. È importante che gli studenti comprendano come utilizzare questi modelli e quando è più opportuno utilizzare un modello rispetto ad un altro.

1. Logistic regression: Cos'è e come si usa questo semplice ma potente algoritmo per classificare testi.

2. Naive Bayes: come utilizzare il teorema di Bayes per classificare il testo in base alla probabilità delle parole che appaiono in un documento rispetto alle categorie.

3. SVM (Support Vector Machines): come utilizzare i vettori di supporto per classificare il testo, identificando i separatori di massima margine tra le categorie.

4. RNN (Rete Neurale Ricorrente): come utilizzare le reti neurali ricorrenti per classificare il testo, tenendo conto dell'ordine e della sequenza delle parole nel testo.

### Introduzione alla classificazione del testo

La classificazione del testo è una tecnica di NLP che serve a categorizzare i documenti in base a determinate proprietà, come la loro appartenenza a una determinata categoria o il loro sentimento. Questa è una delle applicazioni più comuni di NLP, come ad esempio la classificazione di un'email come spam o non spam, o la classificazione di una recensione come positiva o negativa. Ci sono diversi metodi di classificazione del testo, ma tre dei più comuni sono Naive Bayes, SVM (Support Vector Machine) e RNN (Rete Neurale Ricorrente).

Naive Bayes è un algoritmo di classificazione basato sul teorema di 
Bayes, che prevede la probabilità di un evento in base alle probabilità delle sue cause indipendenti. In NLP, questo significa calcolare la probabilità che un documento appartenga a una determinata categoria basandosi sulle parole che compaiono nel documento. L'approccio Naive Bayes considera le parole come indipendenti tra loro, ovvero che la presenza di una parola non influisce sulle probabilità di presenza di un'altra parola, rendendo questo modello facile da implementare e veloce nell'elaborazione. Tuttavia, questa ipotesi di indipendenza tra le parole spesso non è vera e può portare a risultati imprecisi.

SVM (Support Vector Machine) è un altro metodo di classificazione che utilizza una funzione di kernel per trasformare il testo in uno spazio ad alte dimensioni, dove è possibile trovare una separazione lineare tra le categorie. In questo modello, i documenti sono rappresentati come vettori di features, e l'algoritmo cerca il limite di separazione che massimizza la distanza tra le categorie. Questo approccio offre buone prestazioni per molti problemi di classificazione, ma richiede molte risorse computazionali e una configurazione adeguata del kernel.

Infine, RNN (Rete Neurale Ricorrente) è un modello di deep learning che si è dimostrato molto efficace nella classificazione del testo. Questo modello prende in considerazione l'ordine delle parole nel documento e utilizza una rete neurale ricorrente per elaborare le informazioni in modo sequenziale. La rete neurale può essere addestrata su grandi corpus di dati e rappresentare le relazioni tra le parole, offrendo prestazioni molto elevate per molti problemi di classificazione del testo. Tuttavia, l'addestramento di una RNN richiede molte risorse computazionali e un grande corpus di dati etichettati, rendendo questo modello più adatto a grandi organizzazioni con grandi set di dati.

In sintesi, la classificazione del testo è una delle applicazioni più importanti di NLP e può essere realizzata utilizzando diversi metodi, tra cui Naive Bayes, SVM e RNN. Ognuno di questi metodi ha i propri vantaggi e svantaggi, e la scelta dipenderà dalle esigenze specifiche del problema.

### 1. La regressione logistica (Logistic Regression)

La Regressione Logistica è un algoritmo di classificazione che utilizza una funzione logistica per prevedere la probabilità di appartenenza ad una categoria o classe per un dato esempio di input. La funzione logistica produce un valore compreso tra 0 e 1 che rappresenta la probabilità di appartenenza ad una classe specifica.

Nell'ambito del Natural Language Processing (NLP), la Regressione Logistica può essere utilizzata per diversi scopi di classificazione. Ad esempio, può essere utilizzata per classificare documenti in categorie come "spam" o "non-spam", per categorizzare recensioni di prodotti come "positivo" o "negativo" o per classificare le frasi in un dialogo come "affermative" o "domande".

La Regressione Logistica è spesso utilizzata come un punto di partenza (baseline) nella risoluzione di problemi di classificazione del testo e di solito fornisce risultati ragionevoli anche con piccoli dataset di formazione. Tuttavia, non è adatta a problemi di classificazione più complessi che richiedono modelli più sofisticati come le Reti Neurali Artificiali.

Una baseline è un punto di partenza o un punto di riferimento per valutare la prestazione degli algoritmi. In NLP, una baseline comunemente usata è la logistic regression. La logistic regression è utilizzata come baseline poiché è un modello di classificazione molto semplice e lineare, che è molto facile da implementare e che funziona bene con molti problemi di classificazione del testo.

La logistic regression utilizza un modello matematico lineare per prevedere la probabilità che un documento appartenga a una categoria specifica. In NLP, la logistic regression può essere utilizzata per classificare un testo in base alla presenza di parole specifiche nel documento. Il modello addestra i pesi sulle parole che sono indicative della categoria, quindi quando viene eseguita la classificazione, il modello calcola la probabilità che un documento appartenga a una categoria specifica in base alla presenza di quelle parole.

La logistic regression è una buona opzione come baseline poiché è un modello di classificazione molto semplice che può essere utilizzato come punto di partenza per la valutazione degli algoritmi più complessi. Ciò permette di avere una comprensione delle prestazioni minime che possono essere ottenute per un problema specifico, prima di provare metodi più sofisticati.

Per esempio si può implementare un classificatore di testo usando la logistic regression in questo modo:

```python
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.pipeline import make_pipeline
from sklearn.datasets import fetch_20newsgroups

# Scarichiamo i dati necessari
data = fetch_20newsgroups()


# Selezioniamo solo alcune categorie
categorie = ['talk.religion.misc', 'soc.religion.christian', 'sci.space', 'comp.graphics']

# Selezioniamo i sottogruppi del dataset per il training e i test per il modello
train = fetch_20newsgroups(subset='train', categories=categorie)
test = fetch_20newsgroups(subset='test', categories=categorie)

# Creiamo una pipeline
modello = make_pipeline(TfidfVectorizer(), LogisticRegression(random_state=42))

# Facciamo il training del modello
modello.fit(train.data, train.target)

# Fare le previsioni sul set di test
labels = modello.predict(test.data)

# Calcolare l'accuratezza delle previsioni
accuracy = accuracy_score(test.target, labels)
print("Accuratezza: ", accuracy)
```

Nel codice, scarichiamo il dataset `20newsgroups` (ci vorrà un po' solo la prima volta, poi rimane scaricato), e stampiamo tutte le categorie presenti. Dividiamo poi il dataset in training e testing (si fa per controllare magari in seguito l'accuratezza delle predizioni). Creiamo in seguito una pipeline per il modello usando `TfidfVectorizer`che è un metodo per vettorializzare i dati, e con `LogisticRegression` che è il nostro Naive Bayes. Con il metodo `fit()` facciamo il training sui dati, ed esportiamo le predizioni del set di testing nella variabile `labels`.

Dopodiché, facciamo le previsioni sul set di test utilizzando la funzione `predict` e calcoliamo l'accuratezza delle previsioni utilizzando la funzione `accuracy_score` di scikit-learn.

L'accuratezza è circa del'89% con questo algoritmo, dataset e categorie scelti:

```shell
Accuratezza:  0.8924581005586593
```

### 2. Metodo di Naive Bayes

Naive Bayes è un modello di classificazione probabilistico che utilizza il teorema di Bayes per determinare la probabilità di un documento appartenere a una categoria specifica basata sul contenuto del documento. 

```
P (X|Y) = ( P(Y|X) * P(X) ) / P(Y)
```

(Il teorema di Bayes)

Questo modello è stato originariamente sviluppato per il filtraggio delle email ed è oggi uno dei modelli di classificazione più utilizzati per i task di NLP.

Naive Bayes si basa sul principio della probabilità condizionale. Si assume che ciascuna parola in un documento sia indipendente dalle altre parole e che la presenza di una parola nel documento sia un indizio per l'appartenenza a una categoria. Quindi, la probabilità di un documento appartenere a una categoria viene calcolata utilizzando la probabilità condizionale delle parole che compaiono nel documento rispetto alla categoria.

Ci sono tre modelli di Naive Bayes che sono comunemente utilizzati per i task di NLP: Gaussian Naive Bayes, Multinomial Naive Bayes e Bernoulli Naive Bayes. Il modello Gaussian Naive Bayes è utilizzato per le variabili continue, mentre Multinomial e Bernoulli Naive Bayes sono utilizzati per le variabili discrete.

Naive Bayes è un modello efficiente e semplice che funziona bene con grandi quantità di dati e con un gran numero di categorie. Tuttavia, questo modello ha alcune limitazioni, come l'assunzione di indipendenza tra le parole e la mancanza di considerare la relazione tra le parole. Queste limitazioni possono influire sulla precisione del modello.

In sintesi, Naive Bayes è un modello di classificazione semplice ed efficiente che utilizza il teorema di Bayes per determinare la probabilità di un documento appartenere a una categoria specifica basata sul contenuto del documento. Questo modello è stato originariamente sviluppato per il filtraggio delle email ed è oggi uno dei modelli di classificazione più utilizzati per i task di NLP.

Vediamo insieme un esempio di classificazione usando l'algoritmo Multinomial naive Bayes. Useremo il dataset `20 Newsgroups` che è già presente in scikit-learn, anche se in lingua inglese

```python
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import make_pipeline
from sklearn.metrics import accuracy_score

# Scarichiamo i dati necessari
data = fetch_20newsgroups()

# Stampiamo l'elenco completo delle categorie
#print(data.target_names)

# Selezioniamo solo alcune categorie
categorie = ['talk.religion.misc', 'soc.religion.christian', 'sci.space', 'comp.graphics']

# Selezioniamo i sottogruppi del dataset per il training e i test per il modello
train = fetch_20newsgroups(subset='train', categories=categorie)
test = fetch_20newsgroups(subset='test', categories=categorie)

# Creiamo una pipeline
modello = make_pipeline(TfidfVectorizer(), MultinomialNB())

# Facciamo il training del modello
modello.fit(train.data, train.target)

# Applichiamo il modello per predire i dati del sottogruppo test
labels = modello.predict(test.data)

# Calcolare l'accuratezza delle previsioni
accuracy = accuracy_score(test.target, labels)
print("Accuratezza: ", accuracy)

# Possiamo applicare ilmodello per predire anche qualche argomento inventato da noi
# Di seguito alcuni esempi:

prediction = modello.predict(["Sending a shuttle to the ISS"])
print(train.target_names[prediction[0]])

prediction = modello.predict(["Discussing Atheism vs Christianism"])
print(train.target_names[prediction[0]])
```

Nel codice, scarichiamo il dataset `20newsgroups` (ci vorrà un po' solo la prima volta, poi rimane scaricato), e stampiamo tutte le categorie presenti. Dividiamo poi il dataset in training e testing (si fa per controllare magari in seguito l'accuratezza delle predizioni). Creiamo in seguito una pipeline per il modello usando `TfidfVectorizer`che è un metodo per vettorializzare i dati, e con `MultinomialNB` che è il nostro Naive Bayes. Con il metodo `fit()` facciamo il training sui dati, ed esportiamo le predizioni del set di testing nella variabile `labels`.

Dopodiché, facciamo le previsioni sul set di test utilizzando la funzione `predict` e calcoliamo l'accuratezza delle previsioni utilizzando la funzione `accuracy_score` di scikit-learn.

Infine, cerchiamo di predire le categoria per delle frasi inventate da noi, per vedere in maniera empirica se il modello le categorizza bene. Di fatto, in una applicazione pratica questa è la parte che ci interessa di più: applicare il modello a dei dati nuovi, che non siano quelli di training o di testing.

Dopo aver stampato le categorie, le predizioni saranno le seguenti:

```shell
sci.space
soc.religion.christian
```

Da notare che l'accuratezza è intorno all'80% con questo algoritmo, dataset e categorie scelti.

```shell
Accuratezza:  0.8016759776536313
```

### 3. SVM (Support Vector Machines)

Support Vector Machines (SVM) è un algoritmo di classificazione usato in NLP per classificare il testo in diverse categorie. La classificazione viene effettuata identificando i separatori di massima margine tra le categorie, detti vettori di supporto.

La classificazione in SVM avviene sulla base della proiezione delle osservazioni sulle categorie in uno spazio ad alte dimensioni. In questo spazio, le osservazioni possono essere separati da una linea di separazione che divide le diverse categorie. Questa linea è chiamata "iperpiano di separazione" e i vettori che sono più vicini all'iperpiano sono i vettori di supporto.

SVM è particolarmente utile quando il numero di dimensioni è molto elevato rispetto al numero di osservazioni. In questi casi, le osservazioni possono essere facilmente separati in categorie diverse. Inoltre, SVM è anche molto efficiente nella classificazione di osservazioni che sono non linearmente separabili, grazie alla capacità di utilizzare funzioni di kernel.

Per quanto riguarda l'utilizzo di SVM in NLP, il testo viene solitamente rappresentato come un vettore di caratteristiche, ad esempio, utilizzando una rappresentazione BOW o TF-IDF. Questi vettori possono quindi essere usati come input per l'algoritmo SVM per effettuare la classificazione del testo.

In sintesi, SVM è un algoritmo di classificazione potente e flessibile che può essere utilizzato per classificare il testo in diverse categorie. La sua capacità di gestire osservazioni non linearmente separabili, insieme alla sua efficienza computazionale, lo rendono una scelta popolare per molte applicazioni di NLP.

Un esempio di codice che implementa un classificatore di testo in Python
 usando Support Vector Machines (SVM) può essere il seguente:

```python
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.pipeline import make_pipeline
from sklearn.datasets import fetch_20newsgroups

# Scarichiamo i dati necessari
data = fetch_20newsgroups()

# Selezioniamo solo alcune categorie
categorie = ['talk.religion.misc', 'soc.religion.christian', 'sci.space', 'comp.graphics']

# Selezioniamo i sottogruppi del dataset per il training e i test per il modello
train = fetch_20newsgroups(subset='train', categories=categorie)
test = fetch_20newsgroups(subset='test', categories=categorie)

# Creiamo una pipeline
modello = make_pipeline(TfidfVectorizer(), svm.SVC(kernel='linear', C=1, probability=True))

# Facciamo il training del modello
modello.fit(train.data, train.target)

# Fare le previsioni sul set di test
labels = modello.predict(test.data)

# Calcolare l'accuratezza delle previsioni
accuracy = accuracy_score(test.target, labels)
print("Accuratezza: ", accuracy)
```

Nel codice, scarichiamo il dataset `20newsgroups` (ci vorrà un po' solo la prima volta, poi rimane scaricato), e stampiamo tutte le categorie presenti. Dividiamo poi il dataset in training e testing (si fa per controllare magari in seguito l'accuratezza delle predizioni). Creiamo in seguito una pipeline per il modello usando `TfidfVectorizer`che è un metodo per vettorializzare i dati, e con `svm.SVC` che è il nostro Support Vector Machines. Con il metodo `fit()` facciamo il training sui dati, ed esportiamo le predizioni del set di testing nella variabile `labels`.

Dopodiché, facciamo le previsioni sul set di test utilizzando la funzione `predict` e calcoliamo l'accuratezza delle previsioni utilizzando la funzione `accuracy_score` di scikit-learn.

Da notare che l'accuratezza è intorno al 90% con questo algoritmo, dataset e categorie scelti.

```shell
Accuratezza:  0.9175977653631285
```

Notiamo anche come generalmente questo algoritmo, sebbene più accurato di un Naive Bayes, è notevolmente più lento.

Come per l'algoritmo Naive Baies, una volta addestrato il modello, possiamo usarlo per dati che non ha mai visto prima, aggiungendo queste righe finali:

```python
# Possiamo applicare ilmodello per predire anche qualche argomento inventato da noi
# Di seguito alcuni esempi:

prediction = modello.predict(["Sending a satellite into geostationary orbit"])
print(train.target_names[prediction[0]])

prediction = modello.predict(["The best time of the day for photography is the golden hour"])
print(train.target_names[prediction[0]])
```

### 4. RNN (Rete Neurale Ricorrente)

Le Reti Neurali Ricorrenti (RNN) sono un tipo di algoritmi di apprendimento automatico molto efficaci nel classificare i testi. Questi modelli si concentrano sulla relazione tra le parole nel testo e la loro ordine e sequenza. Ciò rende le RNN molto adatte per i task di NLP che richiedono di comprendere il contesto e la struttura del testo.

Le RNN utilizzano una struttura ad anello che consente di mantenere una memoria a lungo termine dei dati precedenti e quindi tener conto della relazione tra le parole nella sequenza. Ad ogni step temporale, la rete riceve una parola come input e elabora l'output sulla base della memoria interna e delle informazioni sulle parole precedenti.

Le RNN possono essere addestrate su un corpus di documenti etichettati con categorie specifiche. Il modello impara a identificare i padroni di relazioni tra le parole nel testo che sono più fortemente associate con le categorie. Queste relazioni sono quindi utilizzate per classificare nuovi documenti in base alla probabilità delle parole che appaiono nel testo rispetto alle categorie.

Un altro importante vantaggio delle RNN è la loro capacità di lavorare con testi di lunghezza variabile. Questo è molto utile nei task di NLP, dove i documenti possono avere lunghezze molto diverse. Le RNN possono elaborare i documenti di lunghezza variabile in modo efficiente, utilizzando la loro memoria interna per tener conto della relazione tra le parole nel testo.

In sintesi, le RNN sono uno strumento molto potente per la classificazione del testo in NLP. Con la loro capacità di comprendere l'ordine e la sequenza delle parole nel testo, le RNN possono fornire una rappresentazione molto precisa delle relazioni semantiche tra le parole e il contesto del testo. Questo rende le RNN una scelta molto valida per i task di classificazione del testo in NLP.

Oltre agli innumerevoli vantaggi, le RNN presentano varie criticità da tenere a mente, ed alcuni svantaggi rispetto a metodi di classificazione più semplici. Vediamo di seguito i problemi più comuni:

1. Vanishing Gradient Problem: Durante il backpropagation, i gradienti possono diventare molto piccoli, rendendo difficile l'addestramento della rete.

2. Problemi di Performance: Le RNN richiedono una quantità enorme di tempo di calcolo e risorse per addestrarsi e utilizzare, rendendole inadatte per grandi set di dati o per applicazioni in tempo reale.

3. Problemi di Overfitting: Le RNN possono soffrire di overfitting, in particolare con piccoli set di dati.

4. Difficoltà nel paralellizzare: Le RNN sono difficili da parallelizzare, rendendo più difficile aumentare la velocità di addestramento o di esecuzione.

5. Problemi di interpretazione: Le reti neurali in generale sono difficili da interpretare, rendendo difficile capire come funzionano e perché danno risultati specifici. Questo rende più difficile affidarsi alle decisioni prese dalle RNN in alcune applicazioni critiche.

Per ognuna di queste criticità si stanno studiando ed implementando delle soluzioni specifiche che è bene conoscere per utilizzare appieno le RNN.

Per il prossimi esempio abbiamo bisogno della libreria keras:

```shell
pip install tensorflow
pip install keras 
```

Adesso passiamo al codice per implementare una RNN usando la libreria Keras per classificare una serie di recensioni di film come positive o negative:

```python
import numpy as np
import keras
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense, LSTM, Embedding
from keras.utils import pad_sequences

(x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=10000)

x_train = pad_sequences(x_train, maxlen=500)
x_test = pad_sequences(x_test, maxlen=500)

model = Sequential()
model.add(Embedding(10000, 32, input_length=500))
model.add(LSTM(100))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(x_train, y_train, batch_size=64, epochs=3, validation_data=(x_test, y_test))

scores = model.evaluate(x_test, y_test, verbose=0)
print("Accuracy: %.2f%%" % (scores[1]*100))
```

In questo esempio, viene utilizzato il dataset IMDB di Keras per 
addestrare un modello di RNN LSTM per classificare le recensioni di film
 come positive o negative. Il modello consiste di un layer di embedding,
 un layer LSTM e un layer denso con una funzione di attivazione sigmoid.
 Il modello viene compilato con una funzione di perdita 
binary_crossentropy e un ottimizzatore Adam, e quindi addestrato sui 
dati di addestramento per tre epoche.

Il programma avrà bisogno di un bel po' di testo e di risorse per il training ed i risultati magari non andranno oltre q quelli del Support Vector Machines.

## Etichettatura delle parti del discorso (Part of speech tagging)

L'etichettatura delle parti del discorso (POS: parts of speech) è un passo importante per molte applicazioni di NLP, poiché fornisce informazioni grammaticali sul testo che possono essere utilizzate in ulteriori elaborazioni. È importante che gli studenti comprendano le diverse tecniche e tool disponibili per l'etichettatura delle parti del discorso e come utilizzarli in modo efficace.

1. Cos'è l'etichettatura delle parti del discorso: il processo di identificare e etichettare la funzione grammaticale di ogni parola in una frase, ad esempio verbo, sostantivo, aggettivo, etc.

2. Tecniche di etichettatura: come utilizzare diverse tecniche, come l'utilizzo di modelli statistici, l'analisi dei contesti e la modellizzazione del linguaggio, per etichettare correttamente le parti del discorso.

3. Tool e librerie: come utilizzare strumenti e librerie di Python, come NLTK e spaCy, per eseguire l'etichettatura delle parti del discorso sui dati di testo.

4. Applicazioni dell'etichettatura delle parti del discorso: come l'etichettatura delle parti del discorso è utilizzata in diverse applicazioni, come l'analisi del sentiment, la traduzione automatica e la sintesi automatica del testo.

### Introduzione alla etichettatura delle parti del discorso

L'etichettatura delle parti del discorso (Part of speech tagging) è una delle prime fasi nell'elaborazione del linguaggio naturale. Il suo obiettivo è identificare la categoria grammaticale di ogni parola all'interno di una frase o di un testo. Queste categorie possono includere nomi, verbi, aggettivi, avverbi e altri. La conoscenza della categoria grammaticale di una parola è importante per molte applicazioni di NLP, tra cui la sintassi, la semantica e la traduzione automatica.

La etichettatura delle parti del discorso è una tecnica di elaborazione del linguaggio naturale che consiste nel categorizzare ogni parola in un testo in base alla sua funzione grammaticale all'interno della frase. La funzione grammaticale di una parola può essere, ad esempio, sostantivo, verbo, aggettivo, avverbio, pronome, preposizione, congiunzione, interiezione, etc. Il processo di etichettatura delle parti del discorso può essere effettuato in modo automatico usando modelli di machine learning. Questi modelli utilizzano diverse informazioni, come la forma della parola, il contesto e la presenza di altre parole vicine, per determinare la categoria grammaticale di una parola. Ad esempio, un modello potrebbe utilizzare la presenza di suffissi specifici per identificare i nomi o potrebbe utilizzare la presenza di altre parole come verbi o avverbi per identificare gli aggettivi.

L'etichettatura delle parti del discorso è un'operazione fondamentale in molte applicazioni di NLP, poiché la funzione grammaticale delle parole è un fattore cruciale per comprendere il significato delle frasi. Ad esempio, l'etichettatura delle parti del discorso è spesso utilizzata per la costruzione di frasi più complesse, la sintesi automatica del testo, la traduzione automatica, l'analisi del sentimento, la identificazione di nomi propri e molte altre applicazioni.

Esistono diversi modelli di etichettatura delle parti del discorso, tra cui la tecnica statistica Hidden Markov Model (HMM) e la tecnica di appren­dimento automatico Conditional Random Field (CRF). Entrambi i modelli utilizzano la probabilità condizionale per determinare la categoria più probabile per ogni parola, tenendo conto del contesto delle parole circostanti.

L'utilizzo di algoritmi di etichettatura delle parti del discorso può migliorare significativamente la precisione delle analisi linguistiche e delle applicazioni NLP. Tuttavia, l'etichettatura delle parti del discorso è anche un'operazione complessa che richiede una comprensione approfondita delle regole grammaticali e una grande quantità di dati di addestramento per ottenere risultati affidabili.

In conclusione, l'etichettatura delle parti del discorso è un'operazione fondamentale nell'elaborazione del linguaggio naturale che aiuta a comprendere il significato delle frasi e a migliorare la precisione delle applicazioni NLP. Esistono diversi modelli di etichettatura delle parti del discorso, tra cui HMM e CRF, che utilizzano la probabilità condizionale per categorizzare le parole in base alla loro funzione grammaticale.

## Named entity recognition (NER)

Il NER è un passo importante per molte applicazioni di NLP, poiché 
fornisce informazioni sui soggetti e gli oggetti presenti in un testo 
che possono essere utilizzati in ulteriori elaborazioni. È importante 
che gli studenti comprendano le diverse tecniche e tool disponibili per 
il NER e come utilizzarli in modo efficace.

1. Cos'è il NER: il processo di identificare e etichettare le entita' denominate, come nomi di persone, luoghi, organizzazioni, date e valute, in un testo.

2. Tecniche di NER: come utilizzare diverse tecniche, come l'utilizzo di modelli statistici, l'analisi dei contesti e la modellizzazione del linguaggio, per eseguire il NER sui dati di testo.

3. Tool e librerie: come utilizzare strumenti e librerie di Python, come NLTK e spaCy, per eseguire il NER sui dati di testo.

4. Applicazioni del NER: come il NER è utilizzato in diverse applicazioni, come la costruzione di knowledge graph, l'analisi delle relazioni tra entita' e l'analisi del sentiment.

## Analisi del sentimento (Sentiment analysis)

L'analisi del sentimento è un'applicazione importante del NLP, poiché 
fornisce informazioni sulle opinioni e le emozioni espresse in un testo.
 È importante che gli studenti comprendano le diverse tecniche e tool 
disponibili per l'analisi del sentimento e come utilizzarli in modo 
efficace.

1. Cos'è l'analisi del sentimento: il processo di determinare la valenza emotiva di un testo, ad esempio se è positivo, negativo o neutro.

2. Tecniche di analisi del sentimento: come utilizzare diverse tecniche, come l'utilizzo di modelli statistici, l'analisi delle parole chiave e la modellizzazione del linguaggio, per eseguire l'analisi del sentimento sui dati di testo.

3. Tool e librerie: come utilizzare strumenti e librerie di Python, come NLTK, spaCy e TextBlob, per eseguire l'analisi del sentimento sui dati di testo.

4. Applicazioni dell'analisi del sentimento: come l'analisi del sentimento è utilizzata in diverse applicazioni, come la valutazione delle opinioni sui prodotti, la monitoraggio della reputazione delle marche e la valutazione delle opinioni su questioni sociali e politiche.

## Riassunto del testo (Text Summarization)

Il summarization del testo è un'applicazione importante del NLP, poiché 
consente di ottenere informazioni essenziali da grandi quantità di testo
 in modo efficiente. È importante che gli studenti comprendano le 
diverse tecniche e tool disponibili per il summarization del testo e 
come utilizzarli in modo efficace.

1. Cos'è il summarization del testo: il processo di creare una versione abbreviata di un testo mantenendo l'essenziale delle informazioni.

2. Tecniche di summarization del testo: come utilizzare diverse tecniche, come l'eliminazione di informazioni ridondanti, l'estrazione di frasi chiave e l'utilizzo di modelli di apprendimento automatico, per eseguire il summarization del testo.

3. Tool e librerie: come utilizzare strumenti e librerie di Python, come Gensim e Sumy, per eseguire il summarization del testo sui dati.

4. Applicazioni del summarization del testo: come il summarization del testo è utilizzato in diverse applicazioni, come la creazione di riassunti automatici di articoli, la generazione di anteprime per i risultati di ricerca e la creazione di resoconti per le riunioni.

## Modelli di seq2seq e generazione del testo

La generazione del testo è un'applicazione avanzata del NLP che utilizza
 i modelli di machine learning per creare testo automaticamente. È 
importante che gli studenti comprendano i concetti fondamentali dei 
modelli seq2seq e come utilizzare i tool e le librerie per eseguire la 
generazione del testo.

1. Cos'è la generazione del testo: il processo di creare testo automaticamente utilizzando modelli di machine learning.

2. Modelli seq2seq: come funzionano i modelli seq2seq, che utilizzano due reti neurali: una encoder e un decoder, per codificare e decodificare le informazioni contenute nel testo.

3. Allenamento dei modelli seq2seq: come allenare i modelli seq2seq su grandi quantità di dati di testo, utilizzando tecniche come la backpropagation e l'ottimizzazione dei parametri.

4. Tool e librerie: come utilizzare strumenti e librerie di Python, come TensorFlow e PyTorch, per eseguire la generazione del testo sui dati.

5. Applicazioni della generazione del testo: come la generazione del testo è utilizzata in diverse applicazioni, come la creazione di chatbot, la generazione di testo automatico e la creazione di sintesi automatiche

## Transformers

I Transformers sono un tipo di modello di Deep Learning che sta emergendo come una delle tecnologie di NLP più avanzate.

1. ntroduzione ai Transformers: spiegare cos'è un Transformer e come differisce da altri modelli di NLP.

2. Architettura dei Transformers: descrivere la struttura di base di un Transformer, incluso il Multi-Head Attention mechanism.

3. Allenamento dei Transformers: descrivere come i Transformers vengono addestrati su grandi quantità di dati di testo, utilizzando tecniche come la backpropagation e l'ottimizzazione dei parametri.

4. Usi dei Transformers: discutere come i Transformers sono utilizzati in diverse applicazioni di NLP, come la classificazione del testo, la named entity recognition e la generazione del testo.

5. Tool e librerie: spiegare come utilizzare strumenti e librerie di Python, come TensorFlow e PyTorch, per addestrare e utilizzare i Transformers per la tua analisi del testo.

I Transformers sono una tecnologia in rapida evoluzione e stanno rivoluzionando il NLP.
