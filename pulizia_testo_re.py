import re

text = "Questo è un esempio di testo. 123, con numeri e punteggiatura!"

# Rimuovi numeri
text = re.sub(r'\d+', '', text)
print(text)
# Output: Questo è un esempio di testo. , con numeri e punteggiatura!

# Rimuovi punteggiatura
text = re.sub(r'[^\w\s]', ' ', text)
print(text)
# Output: Questo è un esempio di testo  con numeri e punteggiatura
# Rimuovi spazi bianchi

text = re.sub(r'\s+', ' ', text)
print(text)
# Output: Questo è un esempio di testo con numeri e punteggiatura 