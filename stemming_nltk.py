import nltk
from nltk.stem import PorterStemmer

# Crea un'istanza di PorterStemmer
stemmer = PorterStemmer()

# Definisci il testo da stemmare
testo = "La corsa del cane era molto veloce. Il gatto correva dietro di lui."

# Tokenizza il testo
parole = nltk.word_tokenize(testo)

# Stemma ogni parola del testo
parole_stemmate = [stemmer.stem(parola) for parola in parole]

# Stampa le parole stemmate
print(parole_stemmate)
